<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
    @if($note_name)
        <p>
            This is an automatic notification to let you know that the note you created referred as "{{ $note_name }}" has been read and was destroyed immediately after.

            Do you want to send another note?
            <a href="https://privnote.com" >here</a>
        </p>
    @else
        <p>
            This is an automatic notification to let you know that the note you created has been read and was destroyed immediately after.

            Do you want to send another note?
            <a href="https://privnote.com" >here</a>
        </p>
    @endif
    </body>
</html>