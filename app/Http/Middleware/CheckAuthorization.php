<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuthorization
{
    protected $apiKey = 'Y3vju0BCLIQV6tNzt5mq5+hO0oyXb4BNtRQdh/KmxYA=';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($_SERVER['HTTP_AUTHORIZATION'] != $this->apiKey)
            return response()->json(array('error'=>'Please set custom header'));
        return $next($request);
    }
}
