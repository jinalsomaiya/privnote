<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class NoteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'note_id' => $this->note_id,
            'name' => $this->name,
            'note' => $this->note,
            'password' => $this->password,
            'destruction_email' => $this->destruction_email,
            'expiry_date' => $this->expiry_date,
            'deleted_at' => $this->deleted_at
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
        ];
    }
}
