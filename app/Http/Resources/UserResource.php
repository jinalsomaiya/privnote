<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'deviceId' => $this->device_id,
            'facebookId' => $this->facebook_id,
        ];
    }

    public function with($request)
    {
        return [
            'success' => true,
        ];
    }
}
