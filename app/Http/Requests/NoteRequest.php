<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'note' => 'required|string',
            'password' => 'string|confirmed',
            'password_confirmation' => 'string',
            'destruction_email' => 'string|email|max:255|unique:notes',
            'hours_for_self_destructs' => 'required'
        ];
    }
}
