<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function loginOrRegister(Request $request)
    {
        $this->validate($request, [
            'deviceId' => 'required|string',
            'password' => 'string'
        ]);

        $user = User::where('device_id', $request->get('deviceId'))->first();

        if (! $user) {
//            if (! Hash::check($request->get('password'), $user->password)) {
//
//                return response()->json([
//                       'success' => false,
//                       'message' => 'password is wrong.'
//                   ]);
//               }

            $user = User::create([
                'device_id' => $request->get('deviceId'),
//                'password' => bcrypt($request->get('password'))
            ]);
        }

        return new UserResource($user);
    }

    public function facebookLogin(Request $request)
    {
        $this->validate($request, [
            'facebookId' => 'required|string'
        ]);

        $user = User::where('facebook_id', $request->get('facebookId'))->first();

        if (! $user) {
            $user = User::create([
                'facebook_id' => $request->get('facebookId')
            ]);
        }

        return new UserResource($user);
    }
}
