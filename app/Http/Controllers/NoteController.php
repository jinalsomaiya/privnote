<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\Http\Resources\NoteResource;
use App\Note;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Hash;

class NoteController extends Controller
{
    public function create(NoteRequest $request)
    {
        $note = Note::create([
            'note_id' => str_random(8),
            'name' => $request->get('name'),
            'note' => $request->get('note'),
            'password' => $request->has('password') ? bcrypt($request->get('password')) : null,
            'destruction_email' => $request->get('destruction_email'),
            'expiry_date' => Carbon::now()->addHours($request->get('hours_for_self_destructs'))
        ]);

        return new NoteResource($note);
    }

    public function destroy(Request $request)
    {
        $this->validate($request, [
            'note_id' => 'required',
        ]);

        $note = Note::whereNoteId($request->get('note_id'))->first();
        if (! $note) {

            return response()->json([
                'success' => false,
                'message' => 'Note not found.'
            ]);
        }
        if ($note->destruction_email) {
            Mail::send('note-email',['note_name' => $note->name], function ($m) use ($note) {
                $m->to($note->destruction_email)->subject($note->name ? 'A note '.$note->name.' has been read' : 'A note you created has been read');
            });
        }
        $note->delete();

        return new NoteResource($note);
    }

    public function show(Request $request)
    {
        $this->validate($request, [
            'note_id' => 'required',
            'password' => 'string'
        ]);

        $note = Note::whereNoteId($request->get('note_id'))->first();
        if (! $note) {

            return response()->json([
                'success' => false,
                'message' => 'Note not found.'
            ]);
        }
        if ($note->password) {
            if (! Hash::check($request->get('password'), $note->password)) {

                return response()->json([
                    'success' => false,
                    'message' => 'password is wrong.'
                ]);
            }
        }

        return new NoteResource($note);
    }
}
