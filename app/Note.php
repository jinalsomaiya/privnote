<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'note_id',
        'name',
        'note',
        'password',
        'destruction_email',
        'expiry_date'
    ];

    protected $dates = ['deleted_at'];
}
