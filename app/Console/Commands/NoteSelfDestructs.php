<?php

namespace App\Console\Commands;

use App\Note;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NoteSelfDestructs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'note : self-destructs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'note destroy at expiry_date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $note = Note::orderBy('expiry_date')->first();
        if (Carbon::now()->eq(Carbon::parse($note->expiry_date))) {
            $note->delete();
        }
    }
}
